
- Contentful Integration
- Cloudinary Integration
- SnipCart Integration
- MailChimp Integration

- Light Mode / Dark Mode using system/browser preferences and store in localStorage
- Reviews store in localStorage
- Service Worker (cachine and PWA)
- CDN for images/static site


Context:
    - App (isNavOpen, isModalOpen, isButtonClicked etc)
    - Base Context with methods to getItems([..]), getItem([..]), writeItems([..]) and writeItems([..])
        Child context's will make use of getItems and getItem for the display of the data before the data is formatted and stored in Blog Context state
        - Blog Context extends Base context
        - Work Context extends Base context
        ...

Search:
    This will loop through the context's and do a getItems storing them with a key of the context
    data = [
        'work' => [
            {id:1, title:'Clear Debt' ... }
            {id:1, title:'Clear Debt' ... }
            {id:1, title:'Clear Debt' ... }
        ],
        'blog' => [
            {id:1, title:'Clear Debt' ... }
            {id:1, title:'Clear Debt' ... }
            {id:1, title:'Clear Debt' ... }
        ]
    ]

Then search will filter each and return the filtered data from state



## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Official Documentation

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy To Vercel, AWS, Hostinger or Netlify

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
Texting 1