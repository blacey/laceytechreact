---
title: "Cookie Policy"
date: "2018-09-18"
---

Cookies are the term given to text files that are stored on your computer by websites to store and track information.

Cookies contain a cookie ID, which is a unique identifier of a specific cookie. It consists of information that needs to be stored and later retrieved by a website. Cookies data can only be read by the web browser that created them, for example, Internet Explorer wouldn't be able to access Google Chrome cookies and vice versa.

## Types of Cookies

- **Session Management** - These cookies are for remembering login details, products within an online shopping cart and anything used to maintain a session between you and the website
- **Personalisation** - These cookies are used to store user preferences for sites, setting and using a different theme on a website and other personalisation settings
- **Tracking** - These cookies are used to record and analyse website visitor behaviour to improve the website

## How are cookies used?

Cookies are often used to track website visitor activity, but they do have other uses. When you visit a website, the web server generates a cookie that acts as an identification card. Every time to revisit the website, your web browser sends the cookie back to the server for processing.

Only the website that generates a cookie can read it, so other web servers are unable to access your information from other cookies. Additionally, web servers can only use the data stored within the cookie, and it doesn't have access to anything else.

### We use cookies for the following purposes

- **Performing Tests**: We use software that allows us to test design changes by showing it to a percentage of website visitors. To ensure the testing results are accurate, a cookie is stored on the users device so they always see the same version of the page being tested.
- **Shopping Cart:** When you add items to your shopping cart a cookie is created on your device keeping track of your cart items.
- **Website Login:** We use cookies to store an authentication token allowing your login details to be remembered by your web browser.
- **Comments:** When leaving a comment on our website you can opt-in for your name and email address to be stored as a cookie. This means that when you visit our website in the future you don't have to type your details in for every comment.

## How Can You Opt-Out From Using Cookies?

By changing the setting of your web browser, you may (at any time), prevent cookies being generated. You can decide if you want first-party cookies and third-party cookies to be enabled or disabled. Cookies that have already been generated can be deleted at any time by clearing the history log for your chosen web browser.

This option is available in all major web browsers like Firefox, Internet Explorer, Google Chrome, Edge and Opera. If the data subject deactivates the setting of cookies in the Internet browser then some parts of our website may not work as they rely on the use of cookies.
