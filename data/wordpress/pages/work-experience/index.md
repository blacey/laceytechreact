---
title: "Work Experience"
date: "2019-11-08"
---

Lacey Tech Solutions are happy to offer students work experience placements. We can get you set up to work on projects from home. Remote working is becoming more mainstream and technology enables teams to work collaboratively and be miles apart from one another.

Working from home requires discipline and good organisation, so building this into the work experience scheme was important.

## Work Experience Roles

**Search Engine Optimisation Analyst** – in this role you will learn about search engine optimisation, keyword research, [website audits,](http://laceytech.local/blog/services/search-engine-optimisation/website-seo-audit/) content analysis, on-page ranking factors and off-page ranking factors.

**Website Designer** – in this role you will learn how to use Adobe Photoshop or Adobe XD to design websites. You will learn about font pairing, grid systems, mobile-first design and user experience.

**Print Designer** – in this role you will learn how to use Adobe Photoshop to design business stationery. You will learn how to design business cards, flyers, brochures, letterheads and promotional materials.

**Social Media Manager** – in this role you will learn about the business side of social media. We will show you tools we use to monitor social channels, manage multiple accounts, schedule content and more.

**Website Server Administrator** – in this role you will be given the task of setting up a server to host a website. You will learn about the Linux operating system, SSH authentication, [server security,](http://laceytech.local/blog/monitor-and-secure-ubuntu-web-server/) server maintenance and SSL security.

**Content Writer** – in this role you will manage and write content for several projects. There is so much content on the Internet that it can be difficult to write engaging content. We teach you how to research topics and brainstorm creative ideas to write meaningful and helpful content for a range of audiences.

**Online Marketing Assistant** – in this role you will learn about different business marketing activities. You will learn what marketing activities are right for a given business and you will be given a chance to help with the marketing of a website.
