---
title: "Improve Forms And Accessibility With HTML 5"
date: "2012-07-06"
categories: 
  - "development"
tags: 
  - "html5"
  - "web-development"
  - "web-forms"
---

Browsers up until now haven't provided a great amount of support for different types of information going into web forms. Now with HTML 5 we have over 12 new form fields that improve usability and accessibility.

The normal process was that a web developer would write the form logic and validation to ensure visitors were entering the correct information. This was often done using a combination of JavaScript and server side validation techniques.

The new HTML5 form elements change all of this and make it easier for developers to make form fields interactive and accessible.

The good news is that newer browsers that understand HTML 5 forms will use the new inputs and attributes. Older web browsers will fallback to the default form inputs providing backwards compatibility.

This means we can use the new HTML 5 form elements today, without inconveniencing people using older web browsers - great stuff!

## What Changes Does HTML 5 Introduce?

- More than 12 new form input types
- Forms now support email fields, numerical input, URLs, dates etc
- News element-level attributes that provide increased interactivity

### New HTML 5 form input types

| Input Type | Purpose | Notes |
| --- | --- | --- |
| tel | For entering a telephone number. | tel does not enforce a particular syntax, so if you want to ensure a particular format, you can use pattern or setCustomValidity() to do additional validation. |
| search | To prompt users to enter text that they want to search for. | The difference between search and text is primarily stylistic. Using an input type of search might result in the input field being styled in a way that is consistent with that platform's search fields. |
| url | For entering a single URL. | url is intended for entering a single website address (absolute URL). |
| email | For entering either a single email address or a list of email addresses. | If the multiple attribute is specified, then multiple email addresses can be entered, separated by commas. |
| datetime | For entering a date and time with the time zone set to UTC. |  |
| date | For entering a date with no time zone. |  |
| month | For entering a date with a year and a month, but no time zone. |  |
| week | For entering a date that consists of a week-year number and a week number, but no time zone. |  |
| time | For entering a time value with hour, minute, seconds, and fractional seconds, but no time zone. |  |
| datetime-local | For entering a date and time with no time zone. |  |
| number | For numerical input | Valid values are floating point numbers (whole and decimal numbers). |
| range | For numerical input, but unlike number, the actual is not important. | The implementation of the range control is a slider in most browsers that support it. |
| color | For choosing color through a color well control. | The value must be a valid lowercase simple color such as #000000. |

### New HTML 5 form input attributes

| Attribute | Purpose | Notes |
| --- | --- | --- |
| autofocus | Focuses the input on the element when the page is loaded. | autofocus can be applied to input, select, textarea, and button. |
| placeholder | Gives the user a hint about what sort of data they should enter. | The placeholder value is displayed in light text until the element gets focus and the user enters some data. It can be specified on input and textarea. |
| form | Specifies one or more forms to which the input element belongs. | By using the form attribute, the input elements can be placed anywhere on the page, not just within the form element. Also, a single input element can be associated with more than one form. |
| required | A boolean attribute that means the element is required. | The required attribute is helpful for doing browser-based validation without using custom JavaScript. |
| autocomplete | For specifying that a field should not autocomplete or be pre-filled by the browser based on a user's past entries. | The autocomplete attribute for fields like a credit card number or one-time password, which you don't want autocomplete. By default, autocomplete is in the on state, so if you want to disable it, set it to off. |
| pattern | For validating an element's value against a regular expression. | When using a pattern, you should also specify a title value to give the user a description of the pattern that's expected. |
| dirname | For submitting the directionality of the control with the form. | For example, if the user entered text data with right-to-left directionality and the input element contained the dirname attribute, then an indication of the right-to-left directionality would be submitted along with the input value. |
| novalidate | For disabling form submission validation when specified on a form element. |  |
| formaction | For overriding the action attribute on the form element. | This attribute is supported on input and button elements. |
| formenctype | For overriding the enctype attribute on the form element. | This attribute is supported on input and button elements. |
| formmethod | For overriding the method attribute on the form element. | This attribute is supported on input and button elements. |
| formnovalidate | For overriding the novalidate attribute on the form element. | This attribute is supported on input and button elements. |
| formtarget | For overriding the target attribute on the form element. | This attribute is supported on input and button elements. |

## Browser-based validation

Let's be honest. Validating form data is a pretty boring task, but you need to do it anyway. To do client-side form validation today, you probably write some custom JavaScript code or use a library to do things like check for valid inputs or ensure required fields are filled out before the form is submitted.

New input attributes like `required` and `pattern` used in combination with CSS pseudo-class selectors make it easier to write the checks and display feedback to the user.

There are other advanced validation techniques that allow you to use JavaScript to set custom validity rules and messages or to determine if an element is invalid and the reason why.

### The required attribute

If the `required` attribute is present, then the field must contain a value when the form is submitted. Here's an example of an input field for a required email address that ensures that the field has a value and that the value is a valid email address.

<input type="email" id="email\_addr" name="email\_addr" required />

### The pattern attribute

The `pattern` attribute specified a regular expression used to validate an input field. This example represents a required text input field for a part number. For the purpose of the example, let's say a part number consists of three uppercase letters followed by four digits. The use of `required` and `pattern` ensure that the field has a value and that the value matches the correct format for a part number. If the user hovers over the field, the message in the title attribute is displayed.

<input type="text" id="part" name="part" required pattern="\[A-Z\]{3}\[0-9\]{4}"
      title="Part numbers consist of 3 uppercase letters followed by 4 digits."/>

Building on the previous example, you can also outline the input field in red if an invalid part number is entered. To do that, add this CSS to put a red border around the input field if the value is invalid.

:invalid {
  border: 2px solid #ff0000;
}

### The formnovalidate attribute

The `formnovalidate` attribute can be applied to `input` or `button` elements. If it's present, then form submission validation is disabled. Here's an example where submitting a form via the Submit button requires valid input, but submitting via the Save button does not.

<input type="text" id="part" name="part" required pattern="\[A-Z\]{3}\[0-9\]{4}"
       title="Part numbers consist of 3 uppercase letters followed by 4 digits."/>
<input type="submit" formnovalidate value="Save">
<input type="submit" value="Submit">
