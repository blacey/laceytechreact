---
title: "Secure WordPress Login Using Google Authenticator"
date: "2013-04-17"
categories: 
  - "wordpress-development"
tags: 
  - "google"
  - "wordpress"
  - "wordpress-security"
---

A few weeks ago we wrote an article about how to improving WordPress security and having read an article about WordPress being hacking we thought we would look at ways to [improve WordPress Security](/shop/wordpress-security-audit/).

We stumbled on a plugin for WordPress that allows you to use the Google Authenticator app when logging into your WordPress website.

The Google Authenticator app provides **2-step authentication** and works on iOS, Android, Windows Phone, webOS, PalmOS, and BlackBerry devices. You need a smartphone to make use of the additional security, but it's worth it.

## How does Google 2-Step Authentication work?

Passwords are easily compromised no matter how complex you make them. This is why most online banking systems either ask for random characters of your password or secret phrase.

Some organisations give you their own authenticator (like Halifax bank and Lloyds Business banking) - these devices generate a random code every 30-60 seconds and you need to enter the code along with your user ID and password in order to login.

If you are using the same password on numerous websites (and lets face it - a lot of you are) then a [security leak on one website](https://http://laceytech.local/pwa/blog/improve-security-for-wordpress-websites/) could put your other online accounts in danger. People are often lazy and don't change their passwords, which is just asking for trouble.

## How To Setup 2-step Authentication in WordPress

In the WordPress dashboard hover over Plugins and click Add New. Use the search bar and find the Google Authenticator WordPress Plugin. Simply click on the Download button and when its finished click Activate.

Next you need to go to hover over Users and click Your Profile. You will now see a section for, Google Authenticator Settings. Click Activate to enable the plugin and enter a description in the box provided.

![Google Authenticator User Settings](images/01-google-authenticator-user-settings.jpg)

Now you need to download the Google Authenticator App for your mobile phone. Once the app has been installed you will have the option to setup a new account.

In our case I called this account "Lacey Tech Blog". On the smartphone there is the option to either enter the secret phrase shown on the user screen or to scan a QR code.

Either method works, but we find scanning the QR code is easier. In the WordPress Users screen click the "Show/Hide QR Code" button and then scan this using the Google Authenticator app on your mobile phone.

Scroll down the WordPress users screen and click "Save Changes".

When you log out of WordPress and re-visit the login screen www.yourwebsite.com/wp-admin you will now see a new box for the Google Authenticator code.

![Google Authenticator WordPress Login](images/02-google-authenticator-wordpress-login.jpg)

Every time you login to your blog you need to enter your WordPress username and password, then open the Google Authenticator App and enter the code that is shown on your phone.

This really helps improve the security of your website but you need your phone to login. You can download backup codes that will allow you to login without needing the authenticator. This is usually in case of emergency or if you have lost your phone.

If you are worried about the security of your WordPress website, you can buy our [WordPress security audit](/shop/wordpress-security-audit/). This goes through all the WordPress security best practices and we make recommendations on further security.
