---
title: "Advanced Guide: How To Improve WordPress Security"
date: "2013-03-08"
categories: 
  - "wordpress-development"
tags: 
  - "wordpress"
  - "wordpress-security"
---

In the last year we've seen a number of WordPress sites fall victim to hackers due to lapse security measures - most of which are easily fixed. This article will provide some **advanced WordPress security** enhancements to protect your WordPress website from attack.

Even with the following security improvements, it's highly unlikely that you will be able to protect your website from hack attempts so we would always suggest you regularly backup your WordPress themes and ensure you make regular WordPress backups.

Security in WordPress is taken very seriously, but as with any other system there are potential security issues that may arise if some basic security precautions aren't taken. This article will go through some common forms of vulnerabilities, and the things you can do to help keep your WordPress installation secure.

## Password Protect The wp-admin Directory

By password protecting the WordPress admin area, if a malicious user tries to access your WordPress admin area login page to launch a brute-force attack, or any other file which resides in the wp-admin directory to send a harmful crafted HTTP request, they are greeted with a server side login prompt.

AuthUserFile /etc/httpd/.htpasswd
AuthType Basic
AuthName "restricted"
Order Deny,Allow
Deny from all
Require valid-user
Satisfy any

This security precaution may be a bit overkill for most people and its not recommended for begginner WordPress users. Alternatively you can install and use a plugin to help maintain security.

## Hide Your WordPress Version

Unknown to most average users of WordPress, there is a HTML tag in the markup of your page called "Generator", and this will show what version of WordPress your website is using.

Normally this isn't an issue if your using the latest version of WordPress but I personally think that having this in the markup of your page is a bad idea.

To remove this from your page and RSS feed markup, simply add the following code to the functions.php file inside your active theme.

remove\_action('wp\_head', 'wp\_generator');

## Install The Login LockDown Plugin

If a hacker is trying to gain access to a WordPress powered website then Simple Login Lockdown prevents brute force login attacks on your WordPress installation. We use and would recommend [Simple Login Lockdown](http://wordpress.org/extend/plugins/simple-login-lockdown/) as this has a variety of customisation options available.

## Delete The Default WordPress Admin User

When you setup a self-hosted WordPress website the default account is 'admin'. Most people don't change this from the default.

It is a potential security risk keeping this account active and in use as most automated scripts try to use a brute force attack to try and guess the password for the admin account.

In WordPress click on "Users" on the left hand side and create a new user account with a strong password and make sure this account has full administrative access.

Once you've done this you can then go back to the user overview screen and delete the admin account.

## Enforce Strong User Passwords

I would highly recommend using different and strong passwords for every website you frequently use. I use a website called SafePass ([safepasswd.com](http://www.safepasswd.com)) to generate random and secure passwords.

If a hacker is using a brute force attack on your WordPress website then it will run through a list of words - if your password used special characters, numbers and letters in a random sequence then it makes it harder for a brute force attack to succeed.

It is recommended that passwords are longer than 12 characters in length but you can always have longer passwords for maximum security. We recommend using a password manager where all the data is encrypted. This gives you the freedom to have complex passwords without needing to remember them.

## Keep WordPress, Plugins and Themes Updated

Like many modern software packages, WordPress is updated regularly to address new security issues that may arise. Improving software security is always an ongoing concern, and to that end you should always keep up to date with the latest version of WordPress.

Older versions of WordPress are not maintained with security updates. I always ensure my hosting clients are running the most up to date version of WordPress, as you can see from [Website Hosting](https://http://laceytech.local/services/website-hosting/).

## Make Regular Website Backups

This isn't really a security tip but it is imperative that you have a working backup of your website. Some hosting providers offer website backups but it is always important to have a backup yourself.

We have seen cases where a web host got hacked and the backups for the sites were deleted along with the live website. This made it impossible to restore and recover from the attack as very few of their clients made their own backups.

The process of backing up your WordPress website isn't a difficult task. There are many different **backup plugins for WordPress** but I use and would recommend.

When backing up your website you need to ensure you backup the following:

1. The Database - This is where your WordPress content is stored
2. The WordPress uploads folder - This is where any uploaded media is stored
3. Your theme directory - These are the files that define how your website looks

If you make regular backups of your website then its a good safeguard if your WordPress website is hacked. In most cases you can restore your website from a backup if its been hacked and change all your user password to reduce the chance of the site being hacked again.

**Do you have any security tips that have helped you in the past?** Feel free to share your thoughts in the comments section below.
