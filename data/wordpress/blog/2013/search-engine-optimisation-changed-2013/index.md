---
title: "Search Engine Optimisation - What Changed In 2013?"
date: "2013-12-24"
categories: 
  - "seo"
tags: 
  - "seo-2"
---

The year is coming to a close and I thought now is a good time to review all the changes that have happened within the world of SEO.

In the past several years, we've been bombarded by a number of key updates in organic search - from Algorithm updates like Penguin, Panda and Hummingbird to inbound link penalties and losing keyword visibility.

It's been a year of change and I hope this article shows you the important of why you should be [monitoring your website](/blog/why-do-websites-need-regular-maintenance/) and SEO on a monthly basis.

As always if you have questions or need help with your SEO please don't hesitate to [get in touch](/contact "Get in touch"). SEO is always evolving and if you don't keep up then your website will quickly fall by the wayside.

## Google rolls out new updates to the Panda algorithm

**_Date: January 2013 - March 2013_**  
The Panda algorithm update was officially launched on the 23rd February 2011 as a way of stopping websites with poor quality content from being shown at the top of Google's search results.

The Panda algorithm updated and re-run periodically to ensure spam/low-quality websites are de-ranked in the search results. In January 2013 Panda was rerun and a lot of sites were affected. Google has been steadily reducing its tolerance for web spam and with every new update more sites fall victim to the Panda Algorithm.

With the latest updates Google are likely to penalize websites that have as little as 50% suspicious links in their back-link profiles, whereas 80% was required when Penguin first launched in 2011.

Google have published guidelines for what it considers to be quality content, which can be read here:  
[http://googlewebmastercentral.blogspot.co.uk/2011/05/more-guidance-on-building-high-quality.html](http://googlewebmastercentral.blogspot.co.uk/2011/05/more-guidance-on-building-high-quality.html)

## Google Penguin Algorithm Update

**_Date: May 2013_**  
Google launched the Penguin Update in April 2012 to better catch sites deemed to be spamming its search results. In particular its looking for sites that buy links or obtain them through link exchanges. When a new Penguin Update is released, sites that have removed bad links using the Google disavow links tool will soon regain lost rankings.

After months of speculation the 4th Penguin update (aka: "Penguin 2.0") was introduced, which moderately impacted the search results. The exact nature of the algorithm update was unclear, but we do know that Penguin 2 was finely targeted to capture more offending spam sites.

## Google Pay Day Loan Update

**_Date: June 2013 - July 2013_**  
Google has updated their search algorithm to target industries where link building and spam tactics are more extreme. Matt Cutts (Head of Google's web spam team) explained the update targets unique link schemes (many of which are illegal).

This update affected approximately 0.3% of USA website queries and was reported to have affected around 4% of Turkish queries were web spam is typically higher.

## PRISM

**_Date: June 2013_**  
PRISM is a tool used by the U.S National Security Agency (NSA) to collect private electronic data belonging to users of major internet services like Gmail, Facebook, Outlook, and others.

PRISM is the latest evolution of the United States government post-9/11 surveillance efforts, which began with the Patriot Act, which was expanded to include the Foreign Intelligence Surveillance Act (FISA) enacted in 2006 and 2007.

On June 6th, The Guardian published reports based on data that was leaked by Edward Snowden. He is a 29-year-old intelligence contractor formerly employed by the NSA and CIA.

Booz Allen Hamilton stated the National Security Agency (NSA) had direct access to servers used by Google, Facebook, Microsoft and other online services.

The news of PRISM will probably see an increase in the use of SSL and HTTPS to secure web traffic, which means SEO's and marketers will have less data to work with.

## Backlinks and Reconsideration Requests

**_Date: June 2013_**  
Google has introduced the Disavow tool to help website owners get rid of poor/bad links in their backlink profile. If your site violates [Google's Quality Guidelines](http://support.google.com/webmasters/bin/answer.py?answer=35769#3) then a spam action may be applied to your website to prevent spam in the search results.

Alerts and violations such as these are shown in Google Webmaster Tools - so make sure you setup and use this service. If your website has been penalised you need to identify poor/bad links pointing to your website and get these links either removed or no-followed.

Once you've done this you can use Google's new Disavow Links tool to deal with remaining unnatural backlinks. This will help if you have been hit by the Penguin algorithm penalty.

## Google Softens Panda Algorithm Penalties

**_Date: July 2013_**  
This update was aimed at making Panda a little less aggressive. When Panda was updated in March we saw that a number of legitimate websites had been targeted and penalised by the Panda update.

Matt Cutts confirmed that the Panda algorithm update will be pushed out on the 1st of every month and will take approximately ten full days to be rolled out everywhere - you will then have a week or so to see the results of that month's update before the process is repeated.

## Google Humming Bird Update

**_Date: August 2013_**  
The Google Hummingbird update has to be the biggest algorithm change to be released, impacting more than 90 percent of searches worldwide.

It's main focus is to allow the Google search engine to do its job easier and quicker by improving semantic search. Google can now show better results for long-tail search queries even if a page is not optimised for them.

## Google Switches to Secure Search

**_Date: September 2013_**  
In October 2011 Google began encrypting searches for anyone who was logged into their Google account to maintain user's privacy. After the upheaval of PRISM in 2013, Google quietly made a change to their site that forced everyone to use their secure search. This meant that all searches made using Google are encrypted - the only exception is for clicks on Google PPC Ads.

Forcing everyone to use the secure search means that [SEO consultants](/locations/farnborough/search-engine-optimisation-farnborough/) can't see keyword data in Google Analytics. Using Google Webmaster Tools allows site owners to see search terms where your website was shown in the search results.

This will continue to be offered, and that will certainly be welcome to soften the blow of losing insights to keywords searches in Google Analytics.

That being said SEO has shifted away from keyword targeting and now requires better understanding of your target audience. On-Page optimisation doesn't start and end with keywords, rather it starts with your target audience and an understanding of what your site visitors expect to see.

## Google Adds Smartphone Crawl Errors to Webmaster Tools

**_Date: December 2013_**  
Google's recent algorithm updates focus on **improving the user experience** for search engine users. They have made significant changes to ensure websites that appear at the top of the search results have quality content.

If you click to view a site that is at the top of page 1 and you can't view the content due to a site error then Google will eventually down-rank that site accordingly.

Now that website owners are focusing more on tablet and mobile versions of their website its important to be able to see and fix errors for these devices. More information on the update can be read here: [Webmaster Tools Smartphone Crawl Update](/blog/google-adds-smartphone-crawl-errors-to-webmaster-tools/).

These are just a handful of changes we've seen this year and 2014 will be no different. What are you thoughts on the changes we've seen this year? Was your website affected? Feel free to share your comments below.
