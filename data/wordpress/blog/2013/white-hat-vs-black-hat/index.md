---
title: "White Hat SEO versus Black Hat SEO"
date: "2013-03-22"
categories: 
  - "seo"
tags: 
  - "black-hat-seo"
  - "seo-2"
  - "white-hat-seo"
---

In the past we've had clients come to us and ask me to get their site ranking for certain keywords within a month. Without a big financial budget and a small team of SEO's, this will rarely happen.

White hat SEO (particularly in boring industries) can be a challenge for new websites. **White hat SEO techniques** require creativity, research, a lot of patience and plenty of elbow grease, but its always worth the effort in the long run.

There was one client in particular who wanted something similar. After telling them their request wasn't possible in their budget they said, "I have been chatting with an SEO company who promised to get me number 1 on Google for all my keywords in a month for under £500". The only way this is possible is using black hat SEO techniques, which are bad practice and is highly discouraged.

The client decided to use the other SEO company and we heard nothing more from them. That was until a few months ago when they contacted us because their website SEO traffic had dropped significantly.

On inspection of their website we found hundreds of links from low-quality websites pointing to their homepage with the link text containing variations of the phrase they wanted to rank for. Like "plumbers in Surrey", "surrey plumbing experts" and "emergency plumbing".

There is no doubt that Google is continuing to improve to efficiency of their search engine results pages (SERPs). This is partially thanks to their Panda and Penguin algorithm updates.  We've seen a dramatic reduction in low-quality websites appearing on page 1 of the search results.

Their update is obviously working well. If you stick to the SEO guidelines then you'll have nothing to worry about. Google and other search engine providers favour good quality sites that are regularly updated.

## What is White Hat SEO

![What is white hat SEO?](images/white-hat-seo.jpg)

**White hat SEO** is where you use accepted SEO practices to improve your website and its position in search engines. Google's algorithm updates (Penguin and Panda) force SEO's to be more imaginative and unique with their link building strategies. Any articles that are written need to be original, pages can't use keyword stuffing and must be at least 300 words and anchor text links need to be selective and relevant.

### Common White Hat SEO Techniques

- **Quality Content** - There is nothing more valuable than quality content for your website. Search engines love to see new content on websites as its an indication that the site is well maintained and up-to-date. Well written content is often shared or linked to by others, which gives search engines positive signals about your site and its content so it will favour your site in the search results. In 2013 we're seeing that social share's influence your ranking within search engines so be sure to spend a good amount of time writing new content for your site.
- **Titles and Meta Data** - Once you've written your content you should write a punchy meta title and meta description to entice users to visit your website from the search results. Don't go overboard with keywords, and make sure your meta description accurately describes the page the visitor will be taken to.
- **Keyword Research and Effective Keyword Use** - Research keywords and key phrases you think people might use to find your content. Single words are not always the most effective target, try multi-word phrases that are much more specific to your product/service and you'll be targeting end users that are much more likely to want what you are offering.
- **Quality Inbound Links** - Having quality and relevant links to your website are always beneficial to improving your position in the search results. Please note my use of the word _**relevant.**_ If your website is about Mobile Phones and you have a link to your site from a local pub that would be a bad link as the pub website isn't relevant to your industry. If you had a link from a Telecommunications company then that would be a better link to have because its relevant to your sites content.
- **Guest blogging** - This is where you write articles that are published on other websites. Doing this helps give your brand or business more exposure online and it can be beneficial if used correctly. You should write unique articles for a range of websites for this to be effective and in doing this you can also build up relationships with other people in your industry. In my opinion building relationships with people is just as important as building back-links to your website.

## What is Black Hat SEO

![What is black hat SEO?](images/black-hat-seo.jpg)

The use of unaccepted or frowned upon SEO practices in order to get higher rankings and more traffic. Use at the risk of being dropped from the engines or at least being removed from high rankings Google's algorithm updates (Panda and Penguin) act as a tag-team, which have hit a lot of the common black hat SEO techniques hard.

Google wants to display good and relevant websites in its search results and any sites that are caught using black hat SEO will get penalised and in most cases their rankings will suffer. An example of this was seen a few weeks ago when [Interflora got penalised by Google](https://http://laceytech.local/blog/interflora-penalised-by-google-for-offering-free-flowers/) for trying to be sneaky about getting backlinks to their website.

### Common Black Hat SEO Techniques

- **Duplicated article submissions** - Don't try to be cheeky and submit the same article to different blogs. Savvy webmasters will often check the content first to make sure it isn't on the internet already. But why would you want to do this anyway? For extra links? We know Panda already penalises sites with duplicated content, so what good is a link when it's ignored.
- **Keyword stuffing** - Don't get trigger-happy with a keyword on one page. Keyword stuffing is suspicious and has recently been proved to have detrimental effects on a website. You should aim for about a 3%-4% keyword density on you landing pages. No more, no less.
- **Link stuffing** - The same applies with links. You're link building should be targeted and concise. Focus all your link juice through one or two keywords. If you link every keyword to various landing pages, you will see no benefit.
- **Content Spinning** - I feel sorry for all those content spinning sites who thought it was a good idea. The re-spun articles make no sense as a result and hold absolutely no value. Don't bother with this cheap trick.
- **Unnatural link building** - Your link profile needs to look natural so don't spam blogs or sites with links pointing to one page. Spread the links out and point them to multiple pages across your site.

## White Hat SEO Is a Long-Term Strategy

There are plenty of short-term benefits when using Black Hat techniques. Keyword stuffing, Unnatural Link Building and other nefarious techniques can improve rankings but these activities are quickly noticed.

So while you may enjoy seeing your website rise in the rankings your smile will quickly fade when you find your website has been booted from the search results.

To give your website the best chance for success website owners should use White Hat SEO techniques. If your chosen SEO company promises quick **number 1 rankings** for your keywords we would run for the hills. .

Good website optimisation takes time and dedication if you want to have lasting results. Through careful and deliberate use of keywords, authoritative guest posting and engaging with people using social media, you stand a better chance of staying ahead of the competition.
