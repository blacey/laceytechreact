---
title: "Courteous Marketing"
date: "2019-11-01"
categories: 
  - "news"
---

## Are You Sticking To GDPR Guidelines?

As I am sure everyone is aware, GDPR has changed the way in which companies communicate with prospects and customers. Failure to comply with GDPR can lead to hefty fines, which no company wants to end up with! 

You can find out more about the GDPR Guidelines through the gov.uk website **_[here](https://www.gov.uk/data-protection)_**.  

### What Is GDPR?

The General Data Protection Regulation is a new digital privacy regulation that was introduced back in May 2018. This regulation means that companies are now required to build in privacy settings to their products and website - having it switched on by default. Due to the fact it is a regulation and not a directive, it is legally binding.   

### **When was it introduced?**

The GDPR was introduced on the 25th May 2018 and supersedes the Data Protection Act. This new approach to data protection was introduced as a way of keeping all companies accountable for their actions. They would no longer be able to exploit personal data for their own gain.  

### What impact with the GDPR have on Marketing?

For marketers, the 3 key areas to worry about are - data permission, data access, and data focus.  

**Data Permission:** This is all about how you manage email opt-ins, where people can request to receive promotional materials from you. You can not just assume they want to be contacted, their consent has to be freely given along with clear affirmative action. For marketers, the best way to make certain people are opting-in is by having a double opt-in. This is where people will tick the box agreeing to be sent marketing materials. But the first email they receive from you should ask them again, are you sure you want to opt into our marketing? By selecting yes they are double opting in, confirming there were no mistakes on their part and they definitely agree.   

**Data Access:** With the introduction of GDPR offering individuals a method to gain more control over how their data is collected and used. This includes the ability to access or remove it - in line with their right to be forgotten. As a marketer, it is your responsibility to make sure that users can easily access their data and remove consent for its use. A simple solution for a marketer is to include an unsubscribe link within your marketing emails which gives users the capabilities to manage their email presence.

**Data Focus:** Marketers can all be guilty of collecting a little more data from a person than we actually need. With GDPR it means now you have to weigh up the data you collect, do you need everything you are asking for, or would it just be nice to know?

For those in marketing who manage email marketing, automation specialists and public relations executives, have to spend that extra bit of time making sure everything they are doing is in line with GDPR. 

### Lacey Tech Solutions

Here at [Lacey Tech Solutions](https://laceytechsolutions.co.uk/) we always keep up to date with the latest laws and regulations. We understand not every business owner has the time to focus solely on their marketing or website maintenance for that matter, hence why our service is here to help you.

If you are looking for support when it comes to your digital marketing, then **call us now 01252 518233** or [get a free quote](https://laceytechsolutions.co.uk/contact/) via our website.
