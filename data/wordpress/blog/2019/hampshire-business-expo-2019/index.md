---
title: "We Attended The Hampshire Business Expo 2019"
date: "2019-03-08"
categories: 
  - "exhibitions"
  - "news"
---

On Tuesday 26th February 2019, we attended the Hampshire Business Expo, which was held at Farnborough International Conference Centre in Farnborough.

![](images/Webp.net-resizeimage-2.jpg)

We entered an Eagle Radio competition to win a place at the exhibition and to our surprise we won! Being our first exhibition, this was an excellent experience that gave us the opportunity to speak to local business owners and fellow exhibitors.

Connect Surrey and Eagle Biz organised the exhibition and right from the start they were emailing useful advice. Incidentally, the Hampshire Business Expo website has been built using [WordPress](/services/wordpress-development/) and they did an excellent job of making it easy for exhibitors to log in and update their information.

Over the last two months, we were busy getting out exhibition handouts designed that showcased our past [SEO success stories](/case-studies/). We even produced frequently asked question handouts for some of our services to help visitors at the show.

With this being our first exhibition, we didn't know what to expect or how to get the best results. Like anything new, we looked at stand design images on Pinterest, watched YouTube videos that offered advice and read a number of blog posts on the subject.

### Who Exhibited?

There were over [80 exhibitors](https://hampshirebusinessexpo.com/exhibitors/exhibitor-list/) at the event and we spoke to a several companies including The Sheriff's Office, Topic of Cancer, Aspire Cleaning, First1Right, IT and General LTD, Aldershot Football Club, Hand Picked Hotels and Privacy Trust.

- ![](images/20190225_160447.jpg)
    
- ![](images/20190225_160502.jpg)
    
- ![](images/20190225_160514.jpg)
    
- ![](images/20190225_160518.jpg)
    
- ![](images/20190225_160558.jpg)
    
- ![](images/20190225_160736.jpg)
    
- ![](images/20190225_160823.jpg)
    
- ![](images/20190225_160839.jpg)
    
- ![](images/20190225_161350.jpg)
    
- ![](images/20190225_161353.jpg)
    
- ![](images/20190226_091715.jpg)
    
- ![](images/20190226_091729.jpg)
    
- ![](images/20190226_091734.jpg)
    
- ![](images/20190226_104259.jpg)
    
- ![](images/20190226_150428.jpg)
    
- ![](images/20190225_140754.jpg)
    
- ![](images/20190225_141912.jpg)
    
- ![](images/20190225_141923.jpg)
    
- ![](images/20190225_143612.jpg)
    
- ![](images/20190225_143622.jpg)
    
- ![](images/20190225_143625.jpg)
    
- ![](images/20190225_143649.jpg)
    
- ![](images/20190225_160310.jpg)
    
- ![](images/20190225_160331.jpg)
    

## Presentations

Visitors to the exhibition were able to register for the Biz Breakfast (that we sadly missed) along with the opportunity of sitting in on some interesting talks throughout the day.

- 8.00 to 10.00 am: Eagle Biz Breakfast Presentation
- 10.15am – 11am: Unleash Your LinkedIn Savviness with LinkedIn Linda
- 11.15am to 12am: Speed Networking Session with Connect Surrey
- 12.30 to 1.30 pm: More Cash, More Profit, More Growth, More Leads with Richard Woods
- 2.15 to 3.15 pm: Speed Networking Session with Omni Networking

## Preparations for the Exhibition

- Choosing Wallpaper and making sure there was enough to cover the booth
- Designing an A1 Sign with our logo and core services
- Designing and Printing leaflets and [case studies](/case-studies/) of past results
- Getting exhibition advice from YouTube and reading the guidelines
- Choosing Sweets for the stand - we went with mints that were unlikely to melt and we chose something without nuts in case of allergies
- We took the time to plan how we were going to decorate our stand and what furniture we would need
- Finding companies to buy or hire out furniture. Don't always go with the recommended suppliers as they worked out to be more expensive
- Planning our follow-up process with prospects after the event
- Reviewing options for data input from business cards
- Think about suitable branded give-away merchandise

## Exhibition Design Templates

Being a design company it would be remiss of us not to mention that we can help you design exhibition marketing materials. These range from roll-up banners to flyers to give you some inspiration if this is your first trade show.

### Roll Up Exhibition Banner Templates

Roll-up exhibition banners are great to show what you offer but they can take up valuable floor space in your booth. These can look good and a lot of exhibitors used these.

[![Search Optimisation Roll-Up Banner Design Template](images/SEO-Rollup-Banner-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/15204/seo-rollup-banner?aff_id=j1203tpgs4)

[![](images/Security-Services-Roll-440x570-1-347x450.jpg)](https://www.template.net/pro/17046/security-services-roll-up-banner?aff_id=j1203tpgs4)

[![Clean Design Roll-Up Banner Template](images/Free-Clean-Style-Roll-Up-Banner-Template-440x570-1-347x450.jpg)](https://www.template.net/editable/1847/free-clean-style?aff_id=j1203tpgs4)

[![](images/Corporate-Roll-440x570-1-347x450.jpg)](https://www.template.net/pro/17034/corporate-roll-up-banner?aff_id=j1203tpgs4)

[![](images/Business-Roll-Up-Banner-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/1844/business-roll-up?aff_id=j1203tpgs4)

[![Creative Agency Roll Up Banner](images/Creative-Agency-Roll-up-Banner-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/15058/creative-agency-rollup-banner?aff_id=j1203tpgs4)

[![Photography Roll Up Exhibition Banner](images/Photography-Roll-440x570-1-347x450.jpg)](https://www.template.net/pro/17042/photography-roll-up-banner?aff_id=j1203tpgs4)

[![](images/School-Promotion-Roll-440x570-1-347x450.jpg)](https://www.template.net/pro/17045/school-promotion-roll-up-banner?aff_id=j1203tpgs4)

### Flyer Templates

Flyers are a great way to visually show what you do to prospects at the exhibition. We found that stapling your business card to the flyer was a good approach.

[![Generic Company Flyer Template](images/Company-Flyer-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/14778/company-flyer?aff_id=j1203tpgs4)

[![Modern Software Flyer Template](images/Modern-Software-Company-Flyer-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/17151/modern-software-company-flyer?aff_id=j1203tpgs4)

[![Consulting Firm Business Flyer Template](images/Consultant-Flyer-Preview-440-347x450.jpg)](https://www.template.net/pro/17836/consultant-flyer?aff_id=j1203tpgs4)

[![Security Company Flyer Template](images/Security-Company-Flyer-Mockup-440-347x450.jpg)](https://www.template.net/pro/17873/security-services-flyer?aff_id=j1203tpgs4)

[![Marketing Business Flyer Template](images/Marketing-Consultant-Flyer-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/12973/marketing-consultant-flyer?aff_id=j1203tpgs4)

[![Big Sale Flyer Template](images/Sales-Flyer-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/12753/sales-flyer?aff_id=j1203tpgs4)

### Business Card Templates

Business cards are an essential marketing asset and we were surprised at how many people didn't have these at the show. We would recommend installing a business card scanner application to save time on data entry.

[![Creative Business Card Template](images/Creative-Business-Card-Template-for-Fashion-Designers-440x570-1-347x450.jpg)](https://www.template.net/pro/14223/creative-business-card-for-fashion-designers?aff_id=j1203tpgs4)

[![Square Business Card Template](images/Creative-Square-Business-Card-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/14281/creative-square-business-card?aff_id=j1203tpgs4)

[![Child Care Business Card Template](images/Child-care-Business-Card-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/16713/child-care-business-card?aff_id=j1203tpgs4)

[![Creative Business Card Template](images/Creative-Student-Business-Card-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/14578/creative-student-business-card?aff_id=j1203tpgs4)

[![Massage Business Card Template](images/Massage-Therapy-Business-Card-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/14586/massage-therapy-business-card?aff_id=j1203tpgs4)

[![Dentist Business Card Template](images/Creative-Dentist-Business-Card-Template-440x570-1-347x450.jpg)](https://www.template.net/pro/14225/creative-dentist-business-card?aff_id=j1203tpgs4)

[Contact us to design materials for your stand](/contact/)

## Exhibition Advice

When speaking with Allana from [The Brand Champion](http://allanafarquhar.co.uk), before the exhibition she stressed that every member of our team had be clear on our messaging. We wrote down a 20-second pitch on who we were and what we do and committed it to memory.

**Planning Your Stand**  
The biggest challenge we had was knowing what to include on our stand. Allana mentioned having an interactive element, so naturally we fired up our Tablet and had our promo video playing on a loop. She said this would be a good way to grab some attention from passers-by.

> "Think of your stand in the same way as your elevator pitch ... you have a very short amount of time to grab the attention of your potential clients so you need make the most out of the space you have!"
> 
> \- Allana Farquhar, The Brand Champion

Another aspect was making sure we had leaflets, business cards, case studies and sweets close to hand.

**How can you collect trade show leads without Wi-Fi?**  
In 2019, the concept of not having Internet connection is a worry for some people, especially at Trade Shows. We know from experience that technology will often fail or Internet access either isn’t available or costs money to use.

Allana makes some very good points on how you can get leads quickly without an Internet connection and without using up delegates valuable time.

- Take a new notepad, pen & stapler - get their business card, staple it to the notepad & then make a note to remind you what you discussed & what specific info they'd like to receive.
- Do similar to the above on your phone or tablet - open a notes app (Evernote or similar), take a photo of their card/badge and make a note for following up.
- Download a free scanning app that works offline such as iCapture which enables you to add notes and even integrate leads with your CRM or email automation after the show.

You can download the [full trade show check-list](http://allanafarquhar.co.uk/) from her website.

We hope this article has helped would-be Trade Show participants to take their first step towards Exhibiting. The most crucial part of any marketing activity is to research and see if the show is going to be attracting your target audience. It has to fit in with your overall marketing strategy for it to be a consideration.

You should track your results at the event, ensure you follow-up with prospects and be able to track the return on your investment. Trade Shows can be expensive, especially for smaller businesses - but they can also help generate leads and revenue for your business when done right.
