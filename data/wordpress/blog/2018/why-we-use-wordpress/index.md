---
title: "Why Do We Use WordPress?"
date: "2018-09-15"
categories: 
  - "wordpress-development"
tags: 
  - "wordpress"
---

In this article, we are going to be talking about why WordPress is our platform of choice when it comes to building websites.

## Why Do We Use WordPress?

There are many factors to consider, and we know WordPress isn't great at everything. Later in the article, we will be discussing our thoughts on improving WordPress even further. We use WordPress because it's easy for our customers to maintain their websites - no technical knowledge is required.

So far there hasn't been a time where we've not been able to build what the client needs within WordPress. Since version 1, WordPress has worked hard to improve the platform, and version 5 is introducing some fantastic new features!

## What is WordPress?

WordPress is a free (open-source) content management system that powers around 30% of all websites worldwide. You can use existing themes and plugins to build a professional website quickly. There are two versions of WordPress, and each has their similarities and differences.

### WordPress.com vs WordPress.org

WordPress started off as a blogging platform, allowing people to publish content online. WordPress.com is a service that will enable you to get a website up and running without needing to worry about website hosting, security and other essential factors.

Having your website set up on WordPress.com has some restrictions. You can’t install plugins to extend the capabilities of your site. Essentially what you get is a website based on an available theme, with pages and the ability to write a company blog.

WordPress.org, on the other hand, is a downloadable version of WordPress that you can use anywhere you want. You can install WordPress on a computer, staging environment or a live web hosting service.

This version of WordPress is unrestricted, meaning that you can install plugins and develop your themes and plugins. If you choose this version of WordPress, you have to consider website hosting, backups, security and DNS.

## What Makes WordPress Great?

### Performance

The high performance and scalable nature of WordPress allow it to be used for anything from a small blogging website to large businesses’ online store websites. Plugins and Themes vary and can cause your website to run slower than expected, so always check users comments and reviews before using it.

WordPress is used by over 30% of all websites on the Internet and companies like Facebook Newsroom, The White House and NGINX all use WordPress and for good reason.

### Plugins

Along with the immense range of features provided by WordPress, you can install plugins to add more features to your website. WordPress easily integrates with plugins like WooCommerce and Customer Relationship Management systems.

There are [a few guidelines for plugin developers](https://en-gb.wordpress.org/plugins/developers/) to restrict what appears on the [plugin directory](https://en-gb.wordpress.org/plugins/):

- The plugin must be compatible with the GNU General Public License v2, or any later version. We strongly recommend using the same licence as WordPress (GPLv2 or later)
- The plugin must not do anything illegal or be morally offensive (that’s subjective, we know)
- You have to use the subversion repository that is given to you for the plugin to show up on the directory
- The plugin must not show a link to an external resource on the front-facing website without explicitly asking the user’s permission (a “powered by” hyperlink for example)
- The plugin must abide by WordPress's list of guidelines. One of the things they look for are spammers and those wanting to abuse the system

### Customisation Options

Wordpress is also extremely useful for teams with its capability of allowing multiple contributors and multiple access levels, which ensures security and usability.

Updates for WordPress improve security and add new features on a regular basis. Plugins and Themes are easy to install and being browser-based means that you can maintain your website without any additional software.

Themes and Plugins allow for full customisation of your WordPress website, and the thousands of choices will enable you to make the site specifically and exactly how you want it.

### WordPress Security

WordPress can develop vulnerabilities (aka security holes), but we often see fixes released through regular updates. The WordPress team and the community work hard to help fix issues and improve the already high standard of security provided by WordPress at its core.

The first place you should look is the [WordPress Security Hardening](https://codex.wordpress.org/Hardening_WordPress) codex entry. This in-depth guide can help ensure that a baseline level of security is met with options to improve this further.

WordPress is spot on with their definition of security. They describe it as, "Security is not an absolute, it's a continuous process and should be managed as such. Security is about risk reduction, not risk elimination, and risk will never be zero".

We would go a step further and say that security is only as strong as your weakest link. For WordPress, it would seem that its weakest link is their plugin and theme review process.

You should only install plugins that are made by reputable developers or companies. Look at the plugin reviews, comments and number of installs before installing.

> "Insecure plugins are one of the most common entry points for hackers to gain access to WordPress blogs."
> 
> Source: [RicksDailyTips.com](https://www.ricksdailytips.com/wp-slimstat-plugin-flaw/)
> 
> Source: [RicksDailyTips.com](https://www.ricksdailytips.com/wp-slimstat-plugin-flaw/)

### Managing Your Content

For any website, you need to produce good content, and WordPress’ easy-to-use content management system and content editor makes it easy to put your content onto your website and makes it easy to improve the Search Engin Optimisation of your site’s pages. The only real limitation of WordPress is your imagination and creativity.

### Speed and Availability

WordPress’ overall speed and availability are defined by that of the server support it. However, WordPress unites with the server and runs at the speed of the server, meaning it can be greatly expanded and upgraded according to the website’s use. The high availability nature of WordPress minifies downtime and means that the website is as available online as the server is.

### Plugin and Theme Support

WordPress is highly used and very popular in the world of websites, and with the popularity of it, comes a community. Along with the community backing, there are also regular updates to WordPress, to improve and make it better.

Although there is no official support option from the WordPress developers themselves, simply Google searching any problems or questions you have will bring up hundreds and thousands of articles and web pages written by members of WordPress’ community.

### WordPress Licencing

The WordPress code is made available under the GPLv2 licence, which gives you the rights to do almost anything with the software. You are allowed to use the software for any purpose and make modifications as you like, along with the availability to redistribute the code, either as the modified or original version.

The licencing allows website and business owners to create a website that is unique and tailored to your needs. You can read all about the [WordPress Licence Terms](https://codex.wordpress.org/License) on their website.

### Search Engine Optimisation

Out of the box, WordPress doesn't have any search engine optimisation features. There are people who don't need SEO and that is why WordPress hasn't provided this as part of its core.

Using an SEO plugin can bridge the gap and improves WordPress for the search engines (Google or Bing). There are many plugins to choose from but we choose to use [Yoast WordPress SEO](https://en-gb.wordpress.org/plugins/wordpress-seo/).

We had two websites using WordPress and our SEO work for [rugmart.co.uk](/case-studies/rugmart/) resulted in a 113% increase in search traffic, and our work for [flowersexpress.co.uk](/case-studies/flowers-express/) increased their search traffic by 138%.

## What Would We Change About WordPress?

**Automated Plugin Testing  
**We can't help but feel that WordPress would benefit from having an automatic testing tool. It would verify that the plugin or theme has implemented the best practices set out by WordPress before being passed to a human for further verification.

**Tighter Restrictions on Theme and Plugin Acceptance  
**At the time of writing this article, there are 56,025 plugins for WordPress, which is a staggering number. WordPress has certain guidelines on the acceptance of themes and plugins, but we feel that tighter restrictions should be in place.

We have seen plugins that exist on the directory where the plugin code is badly written. Some plugins don't include the necessary security checking that prevents Cross-Site Request Forgery.

> "Cross-Site Request Forgery (CSRF) is an attack that forces an end user to execute unwanted actions on a web application in which they're currently authenticated."
> 
> Source: [owasp.com](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF))
> 
> Source: [owasp.com](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF))

**Removal of Old (and Abandoned) Plugins and Themes  
**We have seen a lot of plugins listed in the directory that are years out date. It could be that the developers couldn't maintain the code as WordPress grew or they lost interest.

The problem is that old plugins and themes have not been patched to fix security vulnerabilities. Deleting old plugins would mean that site owners could lose functionality that is integral to their website.

The code lives in Subversion code control, and changes are pushed from development to WordPress through a code commit.

WordPress could build a system to check the last code commit date, and if it's been several months without any code updates, then an email could be sent to the developers.

The system could send an email notification asking if developers are supporting their theme or plugin. WordPress could also inform website owners that the theme or plugin is not being supported and it will no longer be available in the WordPress directory.

When a plugin or theme is not being supported then it should be flagged for removal, and be unlisted on the directory. This would reduce the number of active plugins and would safely facilitate the deletion of old plugins and themes.
