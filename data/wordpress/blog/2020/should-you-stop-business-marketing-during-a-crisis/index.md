---
title: "Should You Stop Business Marketing During A Crisis?"
date: "2020-09-09"
---

When the coronavirus hit many businesses had the knee-jerk reaction of cancelling a lot of their Marketing Services.

With businesses closing with consumers being on lockdown, it makes sense right?

Given that businesses that closed people at home they have more time to be reading content analysing their business and thinking about how they can make things better when they return.

We thought we would see a massive drop in visits to our site but instead we've seen more visits since lockdown measures were put in place.

> _"The biggest mistake people make with content marketing ROI is taking a short-term view. The vast majority of advertising and marketing is fleeting. Say you spend $500 on paid search ads today. You’ll get 25 clicks, but tomorrow, you’ll have to spend that money all over again to see any additional value._ _Spend $500 on a helpful blog post optimized for search, and it’ll drive continuous traffic and leads while building trust with your audience. You’ll spend that $500 up front, but afterwards, high-quality content will continue to deliver results for free."_
> **The Story Telling Edge**

## Small Business Marketing Channels

### Content Creation

Now's the perfect time I'm to be thinking about writing content for your business blog getting people engaged with your consent and promoting what you do. You won't benefit in the short term unless you're an e-commerce company because no one can go out and shop for things that are not essential. When the lock down measures are slowly eased the economy will start to build up again and consumers will have more confidence going out and shopping as they used to.

### Newsletter Subscriptions

You may well find the because of your blog posts and content that you create during this time I'm the people might sign up to your newsletter.

This is a great opportunity where you can an increase your email subscribers so that when your business is allowed to trade again you will be able to reach more people and let them know when you're open.

As it stands companies are sending out more emails ever before. For this reason we recommend and only emailing customers once a week with updates from your company.

This will insure that people don't unsubscribe because you're emailing them too much and filling up their inbox.

### Pay Per Click

Things like pay per click where the cost is usually much greater would make sense to drop your budgets during covid-19. Unless you sell online in which case it may make sense to even increase the amount of that you're spending.

### Social Media

You may want to think of social media posts yourself and share these during lockdown to reduce costs. If you work with a respectable company like us, then pausing your services won't be an issue.

You may want to book a consultation call with your social media company to review the posts and give advice. We are scheduling conference calls with companies to assist them.

### Building your professional network

It's important to increase your professional network, this way you will gain more connections and business associates. A great way to achieve this is through 'business social media' such as LinkedIn which allows you to communicate and link with like minded professionals that you have previously worked with, currently work with or if it's someone you know personally. This in-turn increases your business personality as the more you grow your LinkedIn the more likely you are to find more connections.

### Video Marketing

Gone are the days where you need to be tied to your computer with a webcam to do video marketing. Nowadays, most smartphones come with reasonable cameras that can record video.

If you are using a smartphone, we recommend getting a tripod or a mount to keep the camera steady when recording.

Now is a great time to get involved with video creation as it connects with people better than words on a screen. People can see the passion you have for your craft and that can make a memorable first impression.

- Webinars
- Seminars
- Video Interviews
- Online Courses

## Why Should I Continue To Focus On Marketing?

- The demand for high-quality, helpful content has never been greater
- Content is the most logical place to reallocate event marketing and paid media budgets
- Content fuels virtual events
