---
title: "Can PPC Ads Improve Search Engine Optimisation?"
date: "2017-01-29"
categories: 
  - "seo"
---

Today we're discussing how PPC (Pay Per Click) advertising spend can be reinvested to help improve your Search Engine Optimisation efforts.

For many businesses, Pay Per Click (PPC) advertising is one of the most popular aspects of your digital marketing mix. It's easy to see why [Google advertisers](https://support.google.com/adwords/answer/1722066?hl=en) earn an average of $2 for every $1 they spend on paid advertising.

Google displays Pay Per Click adverts in a prominent position at the top of the search engine results page. As a result, they generate average click-through rates of around 8%, compared to just 2% for organic search results.

A lesser-known advantage of pay per click advertising is the improvement it can make to your search engine optimisation efforts, which is what we will be discussing today.

## SEO vs Pay Per Click

With SEO, it takes time for new pages to be indexed and for any changes to be updated. Using Pay Per Click your changes are made instantly and results can be seen quicker. When talking to clients we liken [Pay Per Click](/services/search-engine-optimisation/pay-per-click/) to energy drinks as you only see the benefits when your drink and budget is consumed.

**SEO Overview**

- SEO takes time to take affect
- Gradual process of improvement
- Requires ongoing work
- Residual benefits will be seen even when your budget has stopped

**Pay Per Click Overview**

- Once setup the results are seen almost instantly
- Requires ongoing management to ensure your budget isn't wasted
- Your results stop the moment you pause or stop your campaign

### The Difficulty of Traditional Keyword Research

Keyword research is a notoriously difficult field of digital marketing. Many companies provide lists of popular keywords and phrases for various topics, but their usefulness can be limited – especially for more niche topics. Even if you do find a relevant list of search queries, how can you be sure that these visitors are looking for what you’re offering - let alone are interested in buying your product or service? Furthermore, how do you determine the most popular words or phrases?

## Using PPC To Find Popular Keywords

By using PPC advertising you can gain a better insight into how people are searching for businesses like yours. Through PPC advertising dashboards (like the one offered by Google) you can enter example keywords and view average annual search volumes – this gives you a better indication of how many people are searching for your service or product (and what they type to find it).

### Finding Related SEO Keywords and Phrases

Another advantage of PPC advertising is the potential it gives you for finding relevant associated keywords. When you search for a particular keyword, you will also be shown similar popular searches or variations on your original search query. This gives you a better idea of the search habits of your potential customers, and can also offer inspiration for your site structure and on-page content.

Once your Pay Per Click campaign is active you will be able to answer the following questions:

- What keywords are sending the most traffic to the website?
- What pages and keywords have a good quality score?
- What are the bounce rates, average time on page and exit rates?

Knowing this information helps you find out what keywords drive more traffic so you can target those keywords in your Search Engine Optimisation efforts.

## Use Pay Per Click To Boost Website Link Building

If your company has a blog you can leverage PPC to drive larger audiences to your article. If your content is well written and provides your customers with insightful advice then you could see more social shares and new inbound links going to your article.

This will eventually give your piece of content a boost in search engine rankings that will have lasting results when you reduce or stop your PPC campaign.

## Putting Your Research Into Practice

Once you have used PPC advertising to discover the most popular keywords and related search queries, you can make several improvements to your website itself to boost your SEO efforts.

Firstly, where possible you should look to include the most popular keywords and phrases in your headings and subheadings (without keyword stuffing or affecting readability).

Your PPC keyword research can also influence your site architecture. Search engines like Google group subjects or topics together to offer related keywords or phrases – you should use this information architecture to group your web pages together.

This could be by creating internal links to connect your web pages, creating new paragraphs on content on your web pages, or even by producing whole new web pages based on the suggested keywords.

Does your business use PPC advertising to [generate leads and new sales](/blog/why-is-my-website-failing-to-drive-enquiries-and-sales/)? Have you tapped into this valuable resource for your keyword research and overall SEO efforts?
