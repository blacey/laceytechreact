import React from 'react'
// import '../../styles/Navbar.module.css'

function Navbar({ children }){    
    return (
        <>
            <nav className="navbar">
                {children}
            </nav>
        </>
    )
}

export default Navbar
