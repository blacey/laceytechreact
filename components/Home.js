// Dependencies
import Head from 'next/head'
import React from 'react'

// Components
import PageContent from '../components/elements/PageContent'
import Testimonials from '../components/Testimonials/Testimonials'

// Styles
import '../styles/Home.module.css'


export default class Home extends React.Component {

  render() {
    return (
      <>
        <Head>
          <title>Award Winning Website &amp; Marketing Agency Farnborough</title>
          <meta name="description" content="Lacey Tech is an award winning website and marketing company who are based in Farnborough, Hampshire. They help small to medium sized companies and has years of success stories to share." />
        </Head>

        <section className="hero">
          <div className="container">
            <h1>Making Websites Great Again!</h1>
            <p>Helping small to medium sized businesses leverage the power and convenience of modern technologies.</p>
            <button>Contact Us</button>
          </div>
        </section>

        <PageContent>
          <Testimonials template={'slider'} />

          <section className="why-choose-us">
            <div className="container">
              <h2>Why Choose Us?</h2>
            </div>
          </section>

          <section className="why-choose-us">
            <div className="container">
              <h2>Text / Image component</h2>
            </div>
          </section>

          <section className="why-choose-us">
            <div className="container">
              <h2>Text / Image component</h2>
            </div>
          </section>

          <section className="why-choose-us">
            <div className="container">
              <h2>case study featured component</h2>
            </div>
          </section>
        </PageContent>

      </>
    )
  }
}