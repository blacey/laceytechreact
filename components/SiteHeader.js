import React from 'react'
import Image from 'next/image'
import Navbar from '../components/Navbar'
import Link from 'next/link'

function SiteHeader() {
    return (
      <header className="site-header">
        <div className="container">
          <Link href="/"><a className="nav-link"><Image src="/logo.png" alt="Lacey Tech Logo" width="160px" height="50px" /></a></Link>

          <Navbar>
            <ul className="navbar-nav navbar-expand-lg navbar-expand-md mr-auto">
              <li className="nav-item"><Link href="/"><a className="nav-link">Home</a></Link></li>
              <li className="nav-item dropdown">
                <Link href="/about/"><a className="nav-link dropdown-toggle" id="navbarAboutDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a></Link>

                <div className="dropdown-menu hidden" aria-labelledby="navbarAboutDropdown">
                  <Link href="/about/team/"><a className="dropdown-item">Our Team</a></Link>
                  <Link href="/about/customers/"><a className="dropdown-item">Our Customers</a></Link>
                  <Link href="/technologies/"><a className="dropdown-item">Technologies</a></Link>
                  <Link href="/industries/"><a className="dropdown-item">Industries</a></Link>
                  <Link href="/locations/"><a className="dropdown-item">Service Area</a></Link>
                </div>
              </li>
              <li className="nav-item"><Link href="/work/"><a className="nav-link">Work</a></Link></li>
              <li className="nav-item"><Link href="/services/"><a className="nav-link">Services</a></Link></li>
              <li className="nav-item"><Link href="/case-studies/"><a className="nav-link">Case Studies</a></Link></li>
              <li className="nav-item"><Link href="/blog/"><a className="nav-link">Blog</a></Link></li>
              <li className="nav-item"><Link href="/contact/"><a className="nav-link">Contact</a></Link></li>
            </ul>
          </Navbar>
        </div>
      </header>
    )
}

export default SiteHeader
