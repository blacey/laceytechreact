import Image from 'next/image'
import Link from 'next/link'

import styles from '../../styles/SocialLinks.module.css'
import facebook from './icons/facebook.svg'
import facebookMessenger from './icons/facebook-messenger.svg'
import fourSquare from './icons/foursquare.svg'
import github from './icons/github.svg'
import instagram from './icons/instagram.svg'
import linkedin from './icons/linkedin.svg'
import medium from './icons/medium.svg'
import pinterest from './icons/pinterest.svg'
import rss from './icons/rss.svg'
import slack from './icons/slack.svg'
import tumblr from './icons/tumblr.svg'
import twitter from './icons/twitter.svg'
import whatsapp from './icons/whatsapp.svg'
import youtube from './icons/youtube.svg'

function SocialLinks() {
    return (
        <div className={styles.social__container}>
            <div className="row">
                <Link href="https://facebook.com">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={facebook} alt="Facebook Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

                <Link href="https://twitter.com">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={twitter} alt="Twitter Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

                <Link href="https://linkedin.com">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={linkedin} alt="LinkedIn Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

                <Link href="#">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={pinterest} alt="Pinterest Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

                <Link href="#">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={instagram} alt="Instagram Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

                <Link href="#">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={youtube} alt="Youtube Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

                <Link href="#">
                    <a className="col-xs-1 col-sm-2">
                        <Image src={rss} alt="RSS Social Media Icon" className={styles.social__icon} width="36" height="36" />
                    </a>
                </Link>

            </div>
        </div>
    )
}

export default SocialLinks
