import React from 'react'

// Use router to get the paths for the breadcrumbs
// Add in breadcrumb SEO markup (Schema)

export default function Breadcrumbs(crumbs=[]) {
    return (
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    )
}
