import React from 'react'

function Hero({ heroName, children }) {
    return (
        <section className={`hero ${heroName}`}>
            <div className="container">
                <div className="row d-flex flex-column">
                    {children}
                </div>
            </div>
        </section>
    )
}

export default Hero
