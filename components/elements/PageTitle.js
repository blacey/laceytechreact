import React from 'react'

function PageTitle({ title }) {
    return (
        <header className="page-header">
            <h1>{title}</h1>
        </header>
    )
}

export default PageTitle
