// Dependencies
import React from 'react'
import styles from '../../styles/Testimonials.module.css'

// Data
import testimonialsData from '../../data/testimonials.json'

function Testimonials({ template }) {
    // const view = template ? require(`views/${template}.js`) : require(`views/default.js`)

    const aggregateReviews = testimonialsData.rating;
    const trustpilotReviews = testimonialsData.sources.trustpilot.reviews;
    const googleReviews = testimonialsData.sources.googlemybusiness.reviews;

    const reviews = trustpilotReviews.concat(googleReviews);
    const show = 2;

    return (
        <section className={styles.testimonials}>
            <div className="container">
                <div className="row d-flex flex-column">
                    <h2 className="text-center">Client Testimonials</h2>

                    <div className={styles.aggregateRating}>
                        <p className="text-center">We are rated {(Math.round(aggregateReviews.aggregate * 100) / 100).toFixed(1)} out of 5 based on {aggregateReviews.count} client reviews</p>
                    </div>
                </div>

                <div className="row">
                    {reviews &&
                        <div className={styles.reviews}>
                        {reviews.map((review, index) => {
                            if(index < show){
                                return (
                                    <div key={index} className={styles.review}>
                                        <div className={styles.reviewBody}>
                                            <blockquote>
                                                <cite>{review.name}</cite>
                                                <p>{review.body}</p>
                                            </blockquote>
                                        </div>
                                    </div>
                                )
                            }
                        })}
                        </div>
                    }
                </div>
            </div>
        </section>
    )
}

export default Testimonials
