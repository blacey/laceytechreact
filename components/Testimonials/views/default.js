import React from 'react'

function View() {
    return (
        <div className="text-center">
            This is the default testimonials view
        </div>
    )
}

export default View
