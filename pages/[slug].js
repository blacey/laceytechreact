import React from 'react'
import Hero from '../components/elements/Hero'
import PageContent from '../components/elements/PageContent'

export default function DynamicPage({ props }) {
    return (
        <PageContent>
            <Hero name="blogSingle">
                <h1>Dynamic Page Template!</h1>
                {}
            </Hero>
        </PageContent>
    )
}