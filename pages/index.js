// Dependencies
import Head from 'next/head'
import React from 'react'
import Link from 'next/link'

// Components
import Testimonials from '../components/Testimonials/Testimonials'

// Page Elements
import Hero from '../components/elements/Hero'
import PageContent from '../components/elements/PageContent'


// Styles
import '../styles/Home.module.css'


export default function Home() {

  return (
    <>
      <Head>
        <title>Award Winning Website &amp; Marketing Agency Farnborough</title>
        <meta name="description" content="Lacey Tech is an award winning website and marketing company who are based in Farnborough, Hampshire. They help small to medium sized companies and has years of success stories to share." />
      </Head>

      <Hero heroName="home">
          <h1>Making Websites Great Again!</h1>
          <p>Helping small to medium sized businesses leverage the power and convenience of modern technologies.</p>
          <Link href="/contact"><a className="btn btn-secondary">Contact Us</a></Link>
      </Hero>

      <PageContent>
        <Testimonials template="slider" />

        <section className="why-choose-us">
          <div className="container">
            <div className="column first">
              <h2>Lacey Tech Website Agency</h2>
              <p>We are a Farnborough based award-winning website &amp; website optimisation company. We focus on helping small – medium sized businesses get results.</p>
              <p>Our team are professional an"d enthusiastic and our managing director has over [experience] years industry experience. We’ve helped hundreds of UK clients significantly improve their businesses with Search Engine Optimisation.</p>
              <Link href="/about/"><a className="btn btn-primary">Learn More</a></Link>
            </div>

            <div className="column last">
 
            </div>
          </div>
        </section>

        <section className="why-choose-us">
          <div className="container">
            <h2>Section</h2>
          </div>
        </section>

        <section className="why-choose-us">
          <div className="container">
            <h2>Section</h2>
          </div>
        </section>

        <section className="why-choose-us">
          <div className="container">
            <h2>case study featured component</h2>
          </div>
        </section>
      </PageContent>

    </>
  )
}