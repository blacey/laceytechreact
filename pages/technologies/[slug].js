import React from 'react'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'
import Link from 'next/link'
import Image from 'next/image'

export default function TechnologySingle({ technology }) {
    const imageUrl = `/images/placeholder.svg`
    const technologyLogoSrc = `https://res.cloudinary.com/laceytechassets/image/upload/c_scale/f_webp/q_auto/v1/Icons/Technology/react_jxqro0`

    return (
        <PageContent>
            <Hero name="home">
                <h1>Single Technology Page</h1>
            </Hero>

            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-md-8">
                            <h2>About Technology</h2>
                            <p>Occaecat Lorem pariatur cupidatat duis. Et minim aliquip cupidatat eu. Exercitation labore et nulla sunt irure. Velit minim dolor excepteur do qui duis labore ut elit. Labore incididunt duis veniam laboris proident mollit minim nisi velit. Sunt irure fugiat nostrud occaecat non ad proident in nostrud sit voluptate. Sunt culpa deserunt ut sit sit Lorem fugiat ad.</p>
                            <p>Ullamco cillum cillum et veniam velit irure velit ullamco Lorem dolore nulla. Occaecat nisi do et anim anim. Cupidatat veniam nulla amet nulla ad eiusmod deserunt. Incididunt eiusmod aliqua qui consequat et. Eu sit cillum cillum magna minim tempor anim dolore aliqua. Ea mollit eu ea nisi cupidatat minim ipsum enim tempor laboris exercitation deserunt.</p>
                            <p>Ut laboris quis amet ad labore deserunt. Fugiat dolore magna adipisicing cupidatat labore magna exercitation sunt velit nulla excepteur ipsum ipsum aliquip. Sit qui ad qui esse nulla aute aliqua non occaecat ex do anim. Ullamco incididunt eiusmod eu eiusmod veniam amet duis ad veniam non.</p>
                        </div>

                        <div className="col-xs-12 col-md-4">
                            <div className="boxed-image">
                                <Image src={technologyLogoSrc} width={400} height={400} alt="Technology Icon" />
                            </div>
                        </div>
                    </div>
                
                    <div style={{height:'75px'}}></div>

                    <div className="row">
                        <div className="col-xs-12">
                            <h2>Technology Benefits</h2>
                            <p>Occaecat Lorem pariatur cupidatat duis. Et minim aliquip cupidatat eu. Exercitation labore et nulla sunt irure. Velit minim dolor excepteur do qui duis labore ut elit. Labore incididunt duis veniam laboris proident mollit minim nisi velit. Sunt irure fugiat nostrud occaecat non ad proident in nostrud sit voluptate. Sunt culpa deserunt ut sit sit Lorem fugiat ad.</p>
                        </div>
                    </div>

                    <div style={{height:'75px'}}></div>

                    <div className="row">
                        <div className="col-xs-12">
                            <h3>Projects Built With Technology (Carousel)</h3>
                            <p>Occaecat Lorem pariatur cupidatat duis. Et minim aliquip cupidatat eu. Exercitation labore et nulla sunt irure. Velit minim dolor excepteur do qui duis labore ut elit. Labore incididunt duis veniam laboris proident mollit minim nisi velit. Sunt irure fugiat nostrud occaecat non ad proident in nostrud sit voluptate. Sunt culpa deserunt ut sit sit Lorem fugiat ad.</p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-4 p-0">
                            <article className="p-10" key="">
                                <h4><Link href=""><a>Project Name</a></Link></h4>
                                <div className="featured-image">
                                    <Link href="">
                                        <Image src={imageUrl} width={480} height={300} alt="Project Image" />
                                    </Link>
                                </div>
                                <p>Anim non minim proident ad anim incididunt laborum. Anim aute duis enim consequat. Est cillum proident occaecat deserunt cillum tempor labore irure sunt. Est cillum proident occaecat deserunt cillum tempor labore irure sunt.</p>
                                <Link href="/work/project"><a className="btn btn-secondary">Read More</a></Link>
                            </article>
                        </div>

                        <div className="col-4 p-0">
                            <article className="p-10" key="">
                                <h4><Link href=""><a>Project Name</a></Link></h4>
                                <div className="featured-image">
                                    <Link href="">
                                        <Image src={imageUrl} width={480} height={300} alt="Project Image" />
                                    </Link>
                                </div>
                                <p>Anim non minim proident ad anim incididunt laborum. Anim aute duis enim consequat. Est cillum proident occaecat deserunt cillum tempor labore irure sunt. Est cillum proident occaecat deserunt cillum tempor labore irure sunt.</p>
                                <Link href=""><a className="btn btn-secondary">Read More</a></Link>
                            </article>
                        </div>

                        <div className="col-4 p-0">
                            <article className="p-10" key="">
                                <h4><Link href=""><a>Project Name</a></Link></h4>
                                <div className="featured-image">
                                    <Link href="">
                                        <Image src={imageUrl} width={480} height={300} alt="Project Image" />
                                    </Link>
                                </div>
                                <p>Anim non minim proident ad anim incididunt laborum. Anim aute duis enim consequat. Est cillum proident occaecat deserunt cillum tempor labore irure sunt. Est cillum proident occaecat deserunt cillum tempor labore irure sunt.</p>
                                <Link href=""><a className="btn btn-secondary">Read More</a></Link>
                            </article>
                        </div>                        
                    </div>

                    <div style={{height:'75px'}}></div>


                </div>
            </section>
        </PageContent>
    )
}