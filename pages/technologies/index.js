// Dependencies
import Link from 'next/link'
import Head from 'next/head'
import Image from 'next/image'

// Components
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'
import technologies from '../../data/technologies.json'

export const getServerSideProps = async () => {
    return {
        props: {
            technologies: technologies
        }
    }
}

function Technologies({ props }) {
    return (
        <>
            <Head>
                <title>Technologies</title>
            </Head>
            
            <Hero heroName="technologies">
                <h1>Tecnologies</h1>
                {technologies?.description && (<p>{technologies.description}</p>)}

                <Link href="/contact" className="btn btn-secondary"><a>Get Free Quote</a></Link>
            </Hero>

            <PageContent>
                <section className="intro">
                    <div className="container">
                        <p>We use a wide range of technologies at Lacey Tech to build websites for our clients. Each project is unique and based on client requirements, we may recommend certain technologies over others. Our team can work with a range of content management systems and server technology to meet your current and future needs.</p>
                    </div>
                </section>

                <section className="technologies">
                    <div className="container">
                        <div className="row">
                        {
                            technologies?.map( (item) => {
                                const url = `/technologies/${item.slug}`
                                const cloudinaryUrl = `https://res.cloudinary.com/laceytechassets/image/upload/f_webp/q_auto/ar_4:3,h_250,w_300/v1/${item.icon}`

                                return (
                                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <article className="technology" key={item.slug}>
                                            <h3><Link href={url}><a>{item.name}</a></Link></h3>
                                            {item.icon && (
                                                <div className="featured-image">
                                                    <Link href={url}>
                                                        <Image src={cloudinaryUrl} width={480} height={250} alt={item.name & " Logo"} />
                                                    </Link>
                                                </div>
                                            )}
                                            {/* {item.excerpt && (<div dangerouslySetInnerHTML={{__html: item.excerpt}}></div>)} */}
                                            <Link href={url}><a className="btn btn-secondary">Read More</a></Link>
                                        </article>
                                    </div>
                                )
                            })
                        }
                        </div>
                    </div>
                </section>
            </PageContent>
        </>
    )
}

export default Technologies
