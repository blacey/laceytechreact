import React from 'react'
import Link from 'next/link'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'

function OurTeam() {
    return (
        <>
            <Hero heroName="about">
                <h1>Our Team</h1>
                <p>See the team that help turn your ideas into reality.</p>
                <Link href="/contact" className="button button-primary"><a>Get A Quote</a></Link>
            </Hero>

            <PageContent>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="column first">
                                <h2>Ben Lacey</h2>
                                <p>.....</p>
                            </div>

                            <div className="column last">
                                <img src="https://placehold.it/300x400" />
                            </div>
                        </div>

                        <div className="row">
                            <div className="column first">
                                <h2>Dom Webber</h2>
                                <p>.....</p>
                            </div>

                            <div className="column last">
                                <img src="https://placehold.it/300x400" />
                            </div>
                        </div>

                        <div className="row">
                            <div className="column first">
                                <h2>Caitlin Mackay</h2>
                                <p>.....</p>
                            </div>

                            <div className="column last">
                                <img src="https://placehold.it/300x400" />
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div className="container">
                        <div className="column first">
                            <h2>Medium Business</h2>
                            <p>If you own a medium business, we’re the website developers for you! We have worked with several companies and brands to take their business to the next stage. We take great pride in ensuring the website is done to the best of our ability to ensure your business reaps the rewards from having a website. We want to see you become the next big thing and would love to be a part of the process by creating the best website possible for your brand.</p>
                        </div>

                        <div className="column last">
                            <img src="https://placehold.it/300x400" />
                        </div>
                    </div>
                </section>

            </PageContent>
        </>
    )
}

export default OurTeam
