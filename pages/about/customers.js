import React from 'react'
import Link from 'next/link'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'

function Customers() {
    return (
        <>
            <Hero heroName="about">
                <h1>Who We Work With</h1>
                <p>Learn more about us and the team that works hard on your project.</p>
            <Link href="/contact" className="button button-primary"><a>Get A Quote</a></Link>
            </Hero>

            <PageContent>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text">
                                <h2>Small Business</h2>
                                <p>We specialise in working on small business websites. As a small business ourselves we know how it feels to go through the process of running a small business and the challenges that are faced. We know that a website can make all the difference in taking your business to the next level – which is why we want to help you! We can provide a finance plan to help support you whilst we work on your website. If you’d like to see other small business websites that we’ve worked on previously, check out our Case Studies page.</p>
                            </div>

                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 media">
                                <img src="https://via.placeholder.com/300/FF0000/FFFFFF" />
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div className="container">
                        <div className="row">      
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text">
                                <h2>Medium Business</h2>
                                <p>If you own a medium business, we’re the website developers for you! We have worked with several companies and brands to take their business to the next stage. We take great pride in ensuring the website is done to the best of our ability to ensure your business reaps the rewards from having a website. We want to see you become the next big thing and would love to be a part of the process by creating the best website possible for your brand.</p>
                            </div>

                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 media">
                                <img src="https://via.placeholder.com/300/FF0000/FFFFFF" />
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div className="container">
                        <div className="row">            
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text">
                                <h2>Sole Traders</h2>
                                <p>As a sole trader, we don’t want you to be alone in the process of finding out what’s the best way to showcase your products or service online. We want to help you out with the process of portraying your business in the best way possible that is right for you and your clients. We know the reputation of your business is a big importance to you and we want to ensure that you are informed during the website building process.</p>
                            </div>

                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 media">
                                <img src="https://via.placeholder.com/300/FF0000/FFFFFF" />
                            </div>
                        </div>
                    </div>
                </section>
            </PageContent>
        </>
    )
}

export default Customers
