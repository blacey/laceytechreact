import React from 'react'
import Link from 'next/link'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'

function About() {
    return (
        <>
            <Hero heroName="about">
                <h1>About Lacey Tech</h1>
                <p>Learn more about us and the team that works hard on your project.</p>
                <Link href="/contact" className="button button-primary"><a>Get A Quote</a></Link>
            </Hero>

            <PageContent>
                <section className="intro">
                    <div className="container">
                        <div className="row">
                            <div className="column first">
                                <p>Lacey Tech are an enthusiastic web development company with <strong>over [years_established] years experience</strong> in the industry. Our customers include individuals, businesses owners, charities and agencies throughout the UK and abroad. We have generated more awareness and enquiries for them by <a class="wpil_internal_link" href="/blog/why-is-my-website-failing-to-drive-enquiries-and-sales/">improving their websites</a> and helping them with an ongoing marketing campaign.</p>
                                <p>We are a growing agency that focuses on User Experience, Website Design &amp; Development and Online Marketing. If your website is difficult to use then you risk losing online sales for your <a href="/blog/10-principles-great-e-commerce-website-design/">E-Commerce website</a>. This is why we focus a lot of our energy on perfecting your mobile website and improving website load times.</p>
                                <p>As time goes on, we are helping small businesses by automating parts of the business, improving reliability and allowing them to scale. We are constantly finding new ways to <a class="wpil_internal_link" href="/blog/improve-your-website-with-rich-snippets/">generate more sales and revenue</a> with modern technology and service offerings.</p>
                            </div>

                            <div className="column last">
                                <img src="https://placehold.it/300x400" />
                            </div>
                        </div>
                    </div>
                </section>

                <section className="who-we-work-with">
                    <div className="container">
                        <div className="row">
                            <div className="column first">
                                <h2>Who We Work With</h2>
                                <p>We want to ensure everyone can have the ability of having a website which is why we never turn down a business, no matter how small or large. There is a way to make every website successful in its own right, so we will keep expanding our network of who we work with.</p>
                                <p>We work with a variety of businesses, including small businesses, medium business, large business, charities, retail stores and sole traders. To ensure we meet the expectations of a wide range of companies to help you get the best website possible.</p>
                                <Link href="/about/customers/" className="button button-primary">Read More</Link>
                            </div>

                            <div className="column last">
                                <img src="https://placehold.it/300x400" />
                            </div>
                        </div>
                    </div>
                </section>

                <section className="industries">
                    <div className="container">
                        <div className="row">
                            <h2>Industries We Help</h2>
                            <p>Here at Lacey Tech Solutions we want to ensure we can help everyone as much as possible when it comes to website development and SEO, which is why we keep our horizons broad when we work with different industries.</p>
                            <p>We work with a magnitude of different industries that we help. Industries including, <a href="/industries/flooring/">Flooring</a>, Security, Dentistry, Bike Shops, Home Improvement, <a  href="/industries/electricians/">Electricians</a>, Estate Agents, Glass &amp; Glazing and <a href="/industries/plumbers/">Plumbing</a>, as well as many more. No project is too big or small at Lacey Tech.</p>
                            <Link href="/industries/" className="button button-primary">Read More</Link>
                            
                            <h3>Industry Slider Here</h3>
                        </div>
                    </div>
                </section>

                <section className="work-experience">
                    <div className="container">
                        <div className="row">
                            <h2>Work Experience Program</h2>
                            <p>Here at Lacey Tech Solutions we want to ensure we can help everyone as much as possible when it comes to website development and SEO, which is why we keep our horizons broad when we work with different industries.</p>
                            <p>We work with a magnitude of different industries that we help. Industries including, <a href="/industries/flooring/">Flooring</a>, Security, Dentistry, Bike Shops, Home Improvement, <a  href="/industries/electricians/">Electricians</a>, Estate Agents, Glass &amp; Glazing and <a href="/industries/plumbers/">Plumbing</a>, as well as many more. No project is too big or small at Lacey Tech.</p>

                            <Link href="/about/work-experience/" className="button button-primary">Read More</Link>
                            <h3>Show Work Experience Posts Here</h3>
                        </div>
                    </div>
                </section>
            </PageContent>
        </>
    )
}

export default About
