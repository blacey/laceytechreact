// Dependencies
import React from 'react'
import Head from 'next/head'

// Components
import SiteHeader from '../components/SiteHeader'
import SiteFooter from '../components/SiteFooter'

// Import Global Styles
import '../styles/globals.css'

import { useQuery, gql } from '@apollo/client'
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'

const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: `https://graphql.contentful.com/content/v1/spaces/b6ohdqgzdbw6`,
    headers: {
        'Authorization': `bearer RMcaGqk6TzERns4Z-krHowzP0iikSOlt6ACB5PWB_UA`
    }
})

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        {/* Add Meta Tags, FB Graph and Twitter Cards */}
        {/* Add SEO fields + Schema.org Markup */}
        {/* Add Service Validation Tags */}
        {/* Add Google Tag Manager (Monitor clicks and form submissions) */}
        
        <title>Lacey Tech</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"></link>
        <script defer async src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
      </Head>

      <SiteHeader />
      
      <Component pageProps />

      <SiteFooter />
      
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </>
  )
}

export default MyApp