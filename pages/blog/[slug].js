import React from 'react'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'

export default function BlogSingle() {
    return (
        <PageContent>
            <Hero name="blogSingle">
                <h1>Single Blog Post</h1>
            </Hero>    
        </PageContent>
    )
}
