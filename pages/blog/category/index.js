import React from 'react'
import Link from 'next/link'
import Hero from '../../../components/elements/Hero'
import PageContent from '../../../components/elements/PageContent'

function BlogCategory() {
    return (
        <>
            <Hero heroName="about">
                <h1>Blog Categories</h1>
                <Link href="#" className="button button-primary"><a>Subscribe To Our Newsletter</a></Link>
            </Hero>

            <PageContent>
                <h1>Blog Category Listing</h1>
                <p>Have links to the categories (with images) to /blog/category/[slug].js file</p>
            </PageContent>
        </>
    )
}

export default BlogCategory
