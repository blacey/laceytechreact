import React from 'react'
import Link from 'next/link'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'
import BlogList from '../../components/Blog/BlogList';
import BlogFilters from '../../components/Blog/BlogFilters';

function Blog() {
    return (
        <>
            <Hero heroName="blog">
                <h1>Blog</h1>
                <p>Here we share advice and tutorials to help small business owners.</p>
                <Link href="#" className="btn btn-secondary"><a>Subscribe To Our Newsletter</a></Link>
            </Hero>

            <PageContent>
                <div className="blog-page">
                    <section className="container">
                        <p>In laboris aliqua cupidatat ad commodo occaecat. In exercitation eiusmod non fugiat velit labore. Tempor qui occaecat id commodo voluptate. Anim nostrud in qui et sint adipisicing. Cupidatat eiusmod consequat occaecat proident esse ex laborum excepteur eu consectetur culpa aliquip. Lorem esse elit do nostrud amet exercitation aute consectetur laboris dolore incididunt do anim nulla.</p>
                    </section>
                    
                    <BlogFilters />

                    <BlogList />
                </div>
            </PageContent>
        </>
    )
}

export default Blog