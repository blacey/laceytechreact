import React from 'react'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'

export default function IndustriesSingle() {
    return (
        <PageContent>
            <Hero name="blogSingle">
                <h1>Single Industries Page</h1>
            </Hero>    
        </PageContent>
    )
}