import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'
import industries from '../../data/industries.json'

function Industries() {
    

    return (
        <>
            <Hero heroName="about">
                <h1>Industries</h1>
                <Link href="/contact/"><a className="btn btn-primary">Get Free Quote</a></Link>
            </Hero>

            <PageContent>
                <section className="industries">
                    <div className="container">
                        <div className="row">
                        {
                            industries.map( industry => {
                                const url = `/industries/${industry.slug}`
                                const cloudinaryUrl = `https://res.cloudinary.com/laceytechassets/image/upload/f_webp/q_auto/ar_4:3,h_250,w_300/v1/${industry.featuredImage}`

                                return (
                                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <article className="technology" key={industry.slug}>
                                            <h3><Link href={url}><a>{industry.title}</a></Link></h3>
                                            {industry.featuredImage && (
                                                <div className="featured-image">
                                                    <Link href={url}>
                                                        <Image src={cloudinaryUrl} width={480} height={250} alt={industry.name & " Industry"} />
                                                    </Link>
                                                </div>
                                            )}
                                            {/* {industry.excerpt && (<div dangerouslySetInnerHTML={{__html: industry.excerpt}}></div>)} */}
                                            <Link href={url}><a className="btn btn-secondary">Read More</a></Link>
                                        </article>
                                    </div>
                                )
                            })
                        }
                        </div>
                    </div>
                </section>
            </PageContent>
        </>
    )
}

export default Industries
