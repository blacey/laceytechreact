// Dependencies
import Link from 'next/link'
import Head from 'next/head'
import Image from 'next/image'

// Components
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'
import work from '../../data/work.json'

export const getServerSideProps = async () => {
    return {
        props: {
            work: work
        }
    }
}

function Work({ props }) {
    return (
        <>
            <Head>
                <title>Work</title>
            </Head>
            
            <Hero heroName="work">
                <h1>Past Projects</h1>
                <p>Cillum ea reprehenderit velit nisi est minim est elit laboris minim officia mollit dolore. Cupidatat officia nisi proident nostrud magna proident sunt adipisicing ad consequat.</p>

                <Link href="/contact" className="btn btn-secondary"><a>Get Free Quote</a></Link>
            </Hero>

            <PageContent>
                <section className="intro">
                    <div className="container">
                        <p>Tempor quis dolore ut aliquip quis tempor do ex adipisicing ut deserunt exercitation. Nisi commodo consectetur veniam est mollit nulla adipisicing. Anim aliquip minim elit nisi non. Labore anim nostrud occaecat sit aliquip dolor dolor sit esse dolor eu. Mollit deserunt sunt cillum veniam commodo et velit officia.</p>
                    </div>
                </section>

                <section className="projects">
                    <div className="container">                        
                        {
                            work?.map( (item) => {
                                const url = `/work/${item.slug}`
                                
                                return (
                                    
                                    <div className="row-fluid" key={item.slug}>
                                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">                                                
                                            <div className="featured-image">
                                            <Link href={url}>
                                                    {item.featuredImage ? (                                                        
                                                        <Image src={item.featuredImage} width={500} height={400} alt={item.page & " Project"} />                                                         
                                                    ) : (
                                                        <Image src="/images/placeholder.svg" width={500} height={500} alt={item.page & " Project"} />
                                                    )}
                                                </Link>
                                            </div>                                                
                                        </div>

                                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <h3><Link href={url}><a>{item.title}</a></Link></h3>
                                            {item.excerpt && (<div dangerouslySetInnerHTML={{__html: item.excerpt}}></div>)}
                                            
                                            <Link href={url}><a className="btn btn-secondary">View Project</a></Link>
                                        </div>
                                    </div>
                                    
                                )
                            })
                        }                        
                    </div>
                </section>
            </PageContent>
        </>
    )
}

export default Work
